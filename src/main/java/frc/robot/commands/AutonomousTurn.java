// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.Drivetrain;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;

public class AutonomousTurn extends SequentialCommandGroup {
  /**
   * Creates a new Autonomous Drive that just turns in place. 
   * This will turn a specific distance and turn back.
   *
   * @param drivetrain The drivetrain subsystem on which this command will run
   */
  public AutonomousTurn(Drivetrain drivetrain) {
    addCommands(
        new TurnDegrees(-360, drivetrain),
        new TurnDegrees(360, drivetrain));
  }
}
