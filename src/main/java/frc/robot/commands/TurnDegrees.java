// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.Drivetrain;
import edu.wpi.first.wpilibj2.command.CommandBase;

public class TurnDegrees extends CommandBase {
  private final Drivetrain m_drive;
  private final double m_degrees;
  private double m_initAngle;

  /**
   * Creates a new TurnDegrees. This command will turn your robot for a desired rotation (in
   * degrees) and rotational speed.
   *
   * @param speed The speed which the robot will drive. Negative is in reverse.
   * @param degrees Degrees to turn. Leverages encoders to compare distance.
   * @param drive The drive subsystem on which this command will run
   */
  public TurnDegrees(double degrees, Drivetrain drive) {
    m_degrees = degrees;
    m_drive = drive;
    addRequirements(drive);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // Set motors to stop, read encoder values for starting point
    m_drive.arcadeDrive(0, 0);
    m_drive.resetEncoders();
    m_drive.resetGyro();
    m_initAngle = m_drive.getGyroAngleZ();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double toTurn = Math.abs(m_degrees) - Math.abs(m_drive.getGyroAngleZ());
    double direction = Math.signum(m_degrees);
    double speed = Math.min(0.65, Math.max(0.35, 0.25+(toTurn-30)/225.0));
    m_drive.arcadeDrive(0, direction*speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_drive.arcadeDrive(0, 0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {

    double turned = m_drive.getGyroAngleZ();
    Boolean finished = Math.abs(turned) - Math.abs(m_degrees) > -0.5;
    if (finished) {
      System.out.printf("initAngle: %f turned: %f \n", m_initAngle, turned);
    }
    return finished;
    // return getAverageTurningDistance() >= (inchPerDegree * m_degrees);
  }

}
